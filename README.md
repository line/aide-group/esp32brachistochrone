# esp32brachistochrone

ESP32 firmware providing a REST API for controlling the brachistochrone science outreach setup.

@aideAPI

## A ESP32 micro-controller to drive the brachistochrone board experiment

This software package provides the [ESP32](https://en.wikipedia.org/wiki/ESP32) micro-controller firmware to drive the brachistochrone board experiment.

![Setup view](https://line.gitlabpages.inria.fr/aide-group/esp32brachistochrone/view.png "Setup view")

### Web interface

![HTML web interface](https://line.gitlabpages.inria.fr/aide-group/esp32brachistochrone/setup_brachistochrone_page.png "HTML web interface")

### Doing the experiment

- Stick the three metal balls onto the electromagnets.
- Press the button on the setup of the HTML interface `Run` button and observe the ball fall.
- Press the HTML interface `Get` button at the motion end to get the detected times.

Tutorial video: [https://youtu.be/ekZs-oHr1R8](https://youtu.be/ekZs-oHr1R8)

### Remarks:

- The `Start` and `Stop` command are only used for debug.
- The `Get` output is of the form
```
{
  "route": "brachistochrone", 
  "time": 0.000,        // Current time in second when the `Get` button is pressed.
  "start_time": 0.000,  // Experiment time in second, when the `Run` button is pressed.
  "detected_times": [0.000, 0.000, 0.000] // Relative times of optical sensor detection, after start_time.
}
```
- Result prediction:
  - The [brachistochrone](https://en.wikipedia.org/wiki/Brachistochrone_curve) addresses the following problem: « Given two points A and B in a vertical plane, what is the curve traced out by a point acted on only by gravity, which starts at A and reaches B in the shortest time. »
  - The result is relatively counter-intuitive:
    - It is _not_ the shortest path (i.e., a rectilinear line), because the ball velocity is not optimal.
    - It is _not_ a path with an initial fall to obtain a maximal acceleration, thus a higher velocity.
    - But a rather complex result only obtained by calculation (with the surprising result that the path runs _below_ the final target)

![Qualitative result](./view-running.png "Qualitative result")

## References

- The [science outreach activity and mechanical hardware description](https://pixees.fr/brachistochrone) (in French)
- The [electronic hardware description](https://gitlab.inria.fr/line/aide-group/esp32brachistochrone/-/blob/master/src/hardware.md)
- The [webservice usage description](https://gitlab.inria.fr/line/aide-group/esp32brachistochrone/-/blob/master/src/usage_brachistochrone.md)

<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32brachistochrone'>https://gitlab.inria.fr/line/aide-group/esp32brachistochrone</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/esp32brachistochrone'>https://line.gitlabpages.inria.fr/aide-group/esp32brachistochrone</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32brachistochrone/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/esp32brachistochrone/-/tree/master/src</a>
- Version `1.1.1`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/esp32brachistochrone.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage


- Refer to <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32brachistochrone/-/blob/master/src/usage_brachistochrone.md'>usage_brachistochrone</a>, for the Brachistochrone web service usage.

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>esp32gpiocontrol: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol'>ESP32 firmware providing a REST API for controlling the GPIO interface and higher functions</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Authors

- Éric Pascual&nbsp; <big><a target='_blank' href='mailto:eric.g.pascual@gmail.com'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://twitter.com/ericpobot'>&#128463;</a></big>
- Martine Olivi&nbsp; <big><a target='_blank' href='mailto:martine.olivi@inria.fr'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://www-sop.inria.fr/members/Martine.Olivi/'>&#128463;</a></big>
- Sabrina Barnabé&nbsp; <big><a target='_blank' href='mailto:snjl-contact@gmail.com'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://snjl.fr'>&#128463;</a></big>
- Thierry Viéville&nbsp; <big><a target='_blank' href='mailto:thierry.vieville@inria.fr'>&#128386;</a></big>
