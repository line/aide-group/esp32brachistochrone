// Implements specific functionnalities of a "Brachistochrone" scientific object

#include "handler.hpp"

//
// Brachistochrone service handlers
//

#define PRESS_BUTTON 15

#define SENSOR_1 13
#define SENSOR_2 27
#define SENSOR_3 12

#define MAGNET_1 26
#define MAGNET_2 25
#define MAGNET_3 4

// Implements the on/off button mechamism

unsigned int handle_brachistochrone_off_last_time = 0;

void handle_brachistochrone_button(bool on_else_off)
{
  verbose(on_else_off ? "handle_brachistochrone_on_off(1)" : "handle_brachistochrone_on_off(0)");
  if(!on_else_off) {
    handle_brachistochrone_off_last_time = millis();
  }
  digitalWrite(MAGNET_1, on_else_off);
  digitalWrite(MAGNET_2, on_else_off);
  digitalWrite(MAGNET_3, on_else_off);
}
void handle_brachistochrone_on()
{
  handle_brachistochrone_button(true);
}
void IRAM_ATTR handle_brachistochrone_run()
{
  handle_brachistochrone_button(false);
  setInterval(handle_brachistochrone_on, 1000, 1);
}
void handle_brachistochrone_start()
{
  // Configures the magnet output
  {
    pinMode(MAGNET_1, OUTPUT);
    pinMode(MAGNET_2, OUTPUT);
    pinMode(MAGNET_3, OUTPUT);
    handle_brachistochrone_on();
  }

  // Registers the on/off button mechamism
  {
    pinMode(PRESS_BUTTON, INPUT);
    attachInterrupt(digitalPinToInterrupt(PRESS_BUTTON), handle_brachistochrone_run, RISING);
    handle_brachistochrone_off_last_time = 0;
  }

  // Registers sensor input timing measure
  {
    gpio_digital_timing_start(SENSOR_1, RISING);
    gpio_digital_timing_start(SENSOR_2, RISING);
    gpio_digital_timing_start(SENSOR_3, RISING);
  }
}
void handle_brachistochrone_stop()
{
  detachInterrupt(digitalPinToInterrupt(PRESS_BUTTON));

  gpio_digital_timing_stop(SENSOR_1);
  gpio_digital_timing_stop(SENSOR_2);
  gpio_digital_timing_stop(SENSOR_3);

  digitalWrite(MAGNET_1, 0);
  digitalWrite(MAGNET_2, 0);
  digitalWrite(MAGNET_3, 0);
}
void hande_brachistochrone_get()
{
  // Converts in seconds and skip negative values
  double d[4] = {
    0.001 * handle_brachistochrone_off_last_time,
    0.001 * ((int) gpio_digital_timing_get(SENSOR_1) - (int) handle_brachistochrone_off_last_time),
    0.001 * ((int) gpio_digital_timing_get(SENSOR_2) - (int) handle_brachistochrone_off_last_time),
    0.001 * ((int) gpio_digital_timing_get(SENSOR_3) - (int) handle_brachistochrone_off_last_time)
  };
  for(unsigned int i = 0; i < 4; i++) {
    d[i] = MAX(0, d[i]);
  }
  //
  answer(true, "{\"route\": \"brachistochrone\", \"time\": %.3f, \"start_time\": %.3f, \"detected_times\": [%.3f, %.3f, %.3f]}", fmillis(), d[0], d[1], d[2], d[3]);
}
//
// Service configuration
//

void handle_brachistochrone()
{
  String action = server->arg("action");
  if(action == "start") {
    handle_brachistochrone_start();
  } else if(action == "run") {
    handle_brachistochrone_run();
  } else if(action == "stop") {
    handle_brachistochrone_stop();
  }
  hande_brachistochrone_get();
}
void handle_brachistochrone_page()
{
#include "setup_brachistochrone_page.hpp"
  server->send(200, "text/html", setup_brachistochrone_page);
}
void setup_brachistochrone_service()
{
  server->on("/", HTTP_GET, handle_brachistochrone_page);
  server->on("/brachistochrone", HTTP_POST, handle_brachistochrone);
}
