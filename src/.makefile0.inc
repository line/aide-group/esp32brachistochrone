BUILD += $(patsubst %.html,%.hpp,$(wildcard setup_*_page.html))

setup_%_page.hpp : setup_%_page.html
	@(echo -n "static char setup_$*_page[] = \"" ; tr "\n" " " < $^ | sed 's/"/\\"/g' | sed 's/  */ /g' ; echo "\";") > $@

