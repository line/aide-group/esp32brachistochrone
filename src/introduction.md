@aideAPI

## A ESP32 micro-controller to drive the brachistochrone board experiment

This software package provides the [ESP32](https://en.wikipedia.org/wiki/ESP32) micro-controller firmware to drive the brachistochrone board experiment.

![Setup view](https://line.gitlabpages.inria.fr/aide-group/esp32brachistochrone/view.png "Setup view")

### Web interface

![HTML web interface](https://line.gitlabpages.inria.fr/aide-group/esp32brachistochrone/setup_brachistochrone_page.png "HTML web interface")

### Doing the experiment

- Stick the three metal balls onto the electromagnets.
- Press the button on the setup of the HTML interface `Run` button and observe the ball fall.
- Press the HTML interface `Get` button at the motion end to get the detected times.

Tutorial video: [https://youtu.be/ekZs-oHr1R8](https://youtu.be/ekZs-oHr1R8)

### Remarks:

- The `Start` and `Stop` command are only used for debug.
- The `Get` output is of the form
```
{
  "route": "brachistochrone", 
  "time": 0.000,        // Current time in second when the `Get` button is pressed.
  "start_time": 0.000,  // Experiment time in second, when the `Run` button is pressed.
  "detected_times": [0.000, 0.000, 0.000] // Relative times of optical sensor detection, after start_time.
}
```
- Result prediction:
  - The [brachistochrone](https://en.wikipedia.org/wiki/Brachistochrone_curve) addresses the following problem: « Given two points A and B in a vertical plane, what is the curve traced out by a point acted on only by gravity, which starts at A and reaches B in the shortest time. »
  - The result is relatively counter-intuitive:
    - It is _not_ the shortest path (i.e., a rectilinear line), because the ball velocity is not optimal.
    - It is _not_ a path with an initial fall to obtain a maximal acceleration, thus a higher velocity.
    - But a rather complex result only obtained by calculation (with the surprising result that the path runs _below_ the final target)

![Qualitative result](./view-running.png "Qualitative result")

## References

- The [science outreach activity and mechanical hardware description](https://pixees.fr/brachistochrone) (in French)
- The [electronic hardware description](https://gitlab.inria.fr/line/aide-group/esp32brachistochrone/-/blob/master/src/hardware.md)
- The [webservice usage description](https://gitlab.inria.fr/line/aide-group/esp32brachistochrone/-/blob/master/src/usage_brachistochrone.md)
