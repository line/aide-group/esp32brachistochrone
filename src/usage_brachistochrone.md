# Installing a the EPS32 brachistochrone mechanism

## Web page

- `http://$IP:80/` opens the user web page

## End user usage

- `curl -X POST -d "action=(start|stop|get)" http://$IP:80/brachistochrone` => `{ "action": \"$action\", "times": [$time_1, $time_2, $time_3]}`
  - `start` : Starts the brachistochrone driver
  - `run` : Runs the brachistochrone demo
  - `stop` : Stops the brachistochrone driver
  - `get` : Gets the last brachistochrone data

